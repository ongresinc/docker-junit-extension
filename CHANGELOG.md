# 1.2.15

Features:

* Allow to use custom registry instead of docker.io
* Update docker-java version to 3.2.5
* Use docker-java-transport-httpclient5

# 1.2.14

Fixes:

* Docker instances are not reused

# 1.2.13

Features:

* Copy from container to host

# 1.2.12

Features:

* Allow wait for a sequence of lines in logs
* Update docker-java version to 3.2.1

Fixes:

* Fixed concurrent modification exception
* Input stream was not closed

# 1.2.11

Fatures:

* Allow to specify a sequence of lines to wait for in the logs
* Update docker-java client to 3.2.1

Fixes:

* Close input streams when reading logs

# 1.2.11

Fixes:

* Update docker-java client to 3.2.0-rc4 to avoid Pid serialization error
* Show all containers when listing

# 1.2.10

Features:

* Switch docker client library to docker-java since spotify docker client is unmanteined

Fixes:

* Enable receiving container logs when they are actually printed out

# 1.2.9

Fixes:

* Fixed copy to containers from file and input stream

# 1.2.8

Features:

* Calling close when closing a stream from run/exec

Fixes:

* Fixed stopIfChanged stopping even if not changed
* Fixed hashCode and equals

# 1.2.7

Features:

* Remove container if changed flag

Fixes:

* Fixed copyResourceIn when resource is in a jar
* Added system to hashCode and equals in mounts

# 1.2.6

Fixes:

* Fix container restart when mounting system paths

# 1.2.5

Features

* Support to expand environment variables with system's ones
* Support to mount system folders

# 1.2.4

Features:

* Added copyIn to allow copy files and resources inside the container.

Fixes:

* Fixed reference to be the test class by default
* Fixed mounting a single file when target forlder does not exists.

# 1.2.3

Features:

* Support to store state in created container's labels.
* Enable parallelism when storing state in container's labels

# 1.2.2

Features:

* Allow to remove containers if specified by a JVM property
* Allow to build with java version 1.8 and 1.9+

