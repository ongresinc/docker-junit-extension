/*-
 *  § 
 * docker-junit-extension
 *    
 * Copyright (C) 2019 OnGres, Inc.
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * § §
 */

package com.ongres.junit.docker;

/**
 * Describes a mount to set for a docker container.
 *
 * <p>
 * Mount a volume by specifying a resource path or a system path.
 * If a resource path is specified, preceding it with an @ means parent
 * folder will be mounted. The resource file (or directory tree) will be copied to a temporary
 * folder so file can be modified internally without interfering with actual resources.
 * If a path is not specified the same resource path (or parent path if prefixed with @)
 * will be used. A reference class can be specified to indicate from which class loader the
 * resource will be loaded, by default class loader of docker-junit-extension will be used.
 * </p>
 *
 * @since 1.2.0
 */
public @interface Mount {

  public static final String DEFAULT_TEMP = "";

  public static final class DefaultReference {
  }

  /**
   * the resource to mount
   * (if preceded by an @ the parent folder of that resource will be mounted).
   */
  String value();

  /**
   * the destination path.
   */
  String path() default "";

  /**
   * a reference class that indicate the classloader to use to load the resource to mount.
   */
  Class<?> reference() default DefaultReference.class;

  /**
   * the temporary folder where to copy resources.
   */
  String temp() default DEFAULT_TEMP;

  /**
   * indicates if the path is a system path
   * (the path will be mounted only if it exists).
   */
  boolean system() default false;
}
