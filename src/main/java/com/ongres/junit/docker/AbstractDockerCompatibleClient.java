/*-
 *  § 
 * docker-junit-extension
 *    
 * Copyright (C) 2019 - 2020 OnGres, Inc.
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * § §
 */

package com.ongres.junit.docker;

import static java.util.stream.Collectors.toList;

import java.io.FileReader;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.github.dockerjava.api.model.Identifier;
import com.github.dockerjava.core.NameParser;
import com.github.dockerjava.core.NameParser.HostnameReposName;
import com.github.dockerjava.core.NameParser.ReposTag;
import com.google.common.collect.ImmutableMap;
import org.jooq.lambda.Seq;

abstract class AbstractDockerCompatibleClient {

  protected static final String HELLO_WORLD_IMAGE = System.getProperty(
        DockerApiClient.class.getName() + ".helloWorldImage", "docker.io/amd64/hello-world:linux");
  protected final Map<String, ImageRepositoryConfig> imageRepositoryConfigs;

  public AbstractDockerCompatibleClient(String imageRegistryConfigsPath) {
    if (imageRegistryConfigsPath != null) {
      Properties properties = new Properties();
      try {
        properties.load(new FileReader(imageRegistryConfigsPath));
      } catch (Exception ex) {
        throw new RuntimeException(ex);
      }
      this.imageRepositoryConfigs = Seq.seq(properties.stringPropertyNames())
          .filter(key -> key.endsWith(".url"))
          .map(key -> key.substring(0, key.length() - ".url".length()))
          .collect(ImmutableMap.toImmutableMap(
              name -> name,
              name -> extractImageRepositoryConfig(properties, name)));
    } else {
      this.imageRepositoryConfigs = ImmutableMap.of();
    }
  }

  private ImageRepositoryConfig extractImageRepositoryConfig(Properties properties, String name) {
    return new ImageRepositoryConfig(name,
        properties.getProperty(name + ".url"),
        properties.getProperty(name + ".username"),
        properties.getProperty(name + ".password"),
        properties.getProperty(name + ".path"));
  }

  protected String getImageToPull(String image) {
    final ImageRepositoryConfig imageRepositoryConfig = getImageRepositoryConfig(image);
    final ReposTag reposTag = NameParser.parseRepositoryTag(image);
    final HostnameReposName hostnameReposName = NameParser.resolveRepositoryName(reposTag.repos);
    final String imageToPull = imageRepositoryConfig != null
        ? imageRepositoryConfig.getUrl() + "/" + imageRepositoryConfig.getPath()
          + "/" + (image.startsWith(hostnameReposName.hostname)
            ? image.substring(hostnameReposName.hostname.length() + 1) : image)
        : image;
    return imageToPull;
  }

  protected ImageRepositoryConfig getImageRepositoryConfig(String image) {
    final Identifier identifier = Identifier.fromCompoundString(image);
    final ReposTag reposTag = NameParser.parseRepositoryTag(image);
    final HostnameReposName hostnameReposName = NameParser.resolveRepositoryName(reposTag.repos);
    ImageRepositoryConfig imageRepositoryConfig = imageRepositoryConfigs.get(image);
    if (imageRepositoryConfig == null) {
      final String nameWithoutRepository = image.startsWith(hostnameReposName.hostname)
          ? image.substring(hostnameReposName.hostname.length() + 1) : image;
      imageRepositoryConfig = imageRepositoryConfigs.get(nameWithoutRepository);
      if (imageRepositoryConfig == null) {
        final String nameWithoutRepositoryAndTag = nameWithoutRepository.substring(0,
            nameWithoutRepository.length() - identifier.tag
              .map(String::length)
              .map(length -> length + 1)
              .orElse(0));
        imageRepositoryConfig = imageRepositoryConfigs.get(nameWithoutRepositoryAndTag);
        if (imageRepositoryConfig == null) {
          imageRepositoryConfig = imageRepositoryConfigs.get("*");
        }
      }
    }
    return imageRepositoryConfig;
  }

  protected List<String> createEnvironmentList(Map<String, String> environment) {
    return environment.entrySet().stream().map(this::toEnvString).collect(toList());
  }

  protected String toEnvString(Map.Entry<String, String> environmentEntry) {
    StringBuilder expandedValue = new StringBuilder();
    final String value = environmentEntry.getValue();
    final int size = value.length();
    for (int index = 0; index < size; index++) {
      char character = value.charAt(index);
      if (character == '\\') {
        index++;
        if (index >= size) {
          throw new IllegalStateException();
        }
        expandedValue.append(value.charAt(index));
        continue;
      }
      if (character == '$') {
        StringBuilder environmentVariable = new StringBuilder();
        index++;
        if (index >= size) {
          throw new IllegalStateException();
        }
        character = value.charAt(index);
        if (character == '{') {
          while (true) {
            index++;
            if (index >= size) {
              throw new IllegalStateException();
            }
            character = value.charAt(index);
            if (character == '}') {
              break;
            }
            environmentVariable.append(character);
          }
        }
        String variableValue = System.getenv(environmentVariable.toString());
        if (variableValue != null) {
          expandedValue.append(variableValue);
        }
        continue;
      }
      expandedValue.append(character);
    }
    return environmentEntry.getKey() + "=" + expandedValue.toString();
  }

}
