/*-
 *  § 
 * docker-junit-extension
 *    
 * Copyright (C) 2019 OnGres, Inc.
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * § §
 */

package com.ongres.junit.docker;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.FileSystemNotFoundException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.stream.Stream;

import com.github.dockerjava.api.exception.DockerException;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.io.IOUtils;

public class Container {

  private final DockerCompatibleClient client;
  private final String id;

  protected Container(DockerCompatibleClient client, String id) {
    super();
    this.client = client;
    this.id = id;
  }

  public String getIp() throws DockerException, InterruptedException, IOException {
    return client.getContainerIp(id);
  }

  public int getPort(int internalPort) throws DockerException, InterruptedException, IOException {
    return getPort(internalPort, NetworkProtocol.TCP);
  }

  public int getPort(int internalPort, NetworkProtocol portType)
      throws DockerException, InterruptedException, IOException {
    return client.getContainerBindedPort(id, internalPort, portType);
  }

  public Stream<String> execute(String... args)
      throws DockerException, InterruptedException, IOException {
    return client.execute(id, args);
  }

  /**
   * Copy all resource from the path to the specified internal path.
   */
  public void copyIn(Path path, String internalPath)
      throws DockerException, InterruptedException, IOException {
    try {
      URI resourceUri = path.toUri();

      try (Closeable closeable = createFileSystemIfNotFound(resourceUri)) {
        Path sourcePath = Paths.get(resourceUri);
        Path targetPath = Paths.get(internalPath);
        Files.walkFileTree(sourcePath, new CopyVisitor(sourcePath, targetPath));
      }
    } catch (Exception ex) {
      throw new RuntimeException(ex);
    }
  }

  /**
   * Copy an {@code InputStream} to the specified internal path.
   */
  public void copyIn(InputStream inputStream, String internalPath)
      throws DockerException, InterruptedException, IOException {
    try {
      copyToContainerAsTar(inputStream, internalPath);
    } catch (Exception ex) {
      throw new RuntimeException(ex);
    }
  }

  /**
   * Copy a resource to the specified internal path.
   */
  public void copyResourceIn(String resource, Class<?> reference, String internalPath)
      throws DockerException, InterruptedException, IOException {
    try {
      URI resourceUri = reference.getResource(resource).toURI();

      try (Closeable closeable = createFileSystemIfNotFound(resourceUri)) {
        Path resourcePath = Paths.get(resourceUri);
        Path sourcePath = resourcePath.getParent();
        Path targetPath = Paths.get(internalPath);
        Files.walkFileTree(resourcePath, new CopyVisitor(sourcePath, targetPath));
      }
    } catch (Exception ex) {
      throw new RuntimeException(ex);
    }
  }

  /**
   * Copy all resource in the resource path to the specified internal path.
   */
  public void copyResourcesIn(String resourcesPath, Class<?> reference,
      String internalPath) throws DockerException, InterruptedException, IOException {
    try {
      URI resourceUri = reference.getResource(resourcesPath).toURI();

      try (Closeable closeable = createFileSystemIfNotFound(resourceUri)) {
        Path sourcePath = Paths.get(resourceUri);
        Path targetPath = Paths.get(internalPath);
        Files.walkFileTree(sourcePath, new CopyVisitor(sourcePath, targetPath));
      }
    } catch (Exception ex) {
      throw new RuntimeException(ex);
    }
  }

  /**
   * Copy from the specified internal path to the specified path.
   */
  public void copyOut(String internalPath, Path path)
      throws DockerException, InterruptedException, IOException {
    try (InputStream inputStream = client.copyFromContainer(id, internalPath);
        TarArchiveInputStream tarArchiveInputStream = new TarArchiveInputStream(inputStream)) {
      TarArchiveEntry tarArchiveEntry = tarArchiveInputStream.getNextTarEntry();
      if (tarArchiveEntry == null) {
        throw new IllegalStateException(
            "Can not find any entry in the output");
      }
      final Path rootEntryPath;
      if (tarArchiveEntry.isDirectory()) {
        rootEntryPath = Paths.get(tarArchiveEntry.getName());
        tarArchiveEntry = tarArchiveInputStream.getNextTarEntry();
      } else {
        if (Paths.get(tarArchiveEntry.getName()).isAbsolute()) {
          rootEntryPath = Paths.get(internalPath);
        } else {
          rootEntryPath = Paths.get(internalPath).getFileName();
        }
      }
      for (;
          tarArchiveEntry != null;
          tarArchiveEntry = tarArchiveInputStream.getNextTarEntry()) {
        final Path entryPath = Paths.get(tarArchiveEntry.getName());
        final Path targetPath = path.resolve(rootEntryPath.relativize(entryPath));
        if (tarArchiveEntry.isFile()
            && !tarArchiveEntry.isSymbolicLink()) {
          copyArchive(tarArchiveInputStream, targetPath);
        }
      }
    }
  }

  private void copyArchive(TarArchiveInputStream tarArchiveInputStream, Path targetPath)
      throws IOException {
    Files.createDirectories(targetPath.getParent());
    Files.copy(tarArchiveInputStream, targetPath);
  }

  private Closeable createFileSystemIfNotFound(URI uri) throws IOException, URISyntaxException {
    try {
      Paths.get(uri);
    } catch (FileSystemNotFoundException ex) {
      return FileSystems.newFileSystem(uri, new HashMap<>());
    }
    return () -> { };
  }

  private class CopyVisitor extends SimpleFileVisitor<Path> {
    private final Path sourcePath;
    private final Path targetPath;

    private CopyVisitor(Path sourcePath, Path targetPath) {
      this.sourcePath = sourcePath;
      this.targetPath = targetPath;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
        throws IOException {
      return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
        throws IOException {
      try (InputStream inputStream = file.getFileSystem().provider().newInputStream(file)) {
        copyToContainerAsTar(inputStream,
            targetPath.resolve(sourcePath.relativize(file).toString()).toString());
      }
      return FileVisitResult.CONTINUE;
    }
  }

  private void copyToContainerAsTar(InputStream inputStream, String targetPath) throws IOException {
    try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
      IOUtils.copy(inputStream, byteArrayOutputStream);
      try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(
          byteArrayOutputStream.toByteArray());
          ByteArrayOutputStream tarByteArrayOutputStream = new ByteArrayOutputStream()) {
        try (TarArchiveOutputStream tarArchiveOutputStream = new TarArchiveOutputStream(
            tarByteArrayOutputStream)) {
          tarArchiveOutputStream.setLongFileMode(TarArchiveOutputStream.LONGFILE_GNU);
          TarArchiveEntry tarArchiveEntry = new TarArchiveEntry(targetPath);
          tarArchiveEntry.setSize(byteArrayOutputStream.size());
          tarArchiveOutputStream.putArchiveEntry(tarArchiveEntry);
          IOUtils.copy(byteArrayInputStream, tarArchiveOutputStream);
          tarArchiveOutputStream.closeArchiveEntry();
          tarArchiveOutputStream.finish();
        }
        try (ByteArrayInputStream tarByteArrayInputStream = new ByteArrayInputStream(
            tarByteArrayOutputStream.toByteArray())) {
          copyToContainer(tarByteArrayInputStream, "/");
        }
      }
    } catch (DockerException | InterruptedException ex) {
      throw new RuntimeException(ex);
    }
  }

  private void copyToContainer(InputStream inputStream, String internalPath)
      throws DockerException, InterruptedException, IOException {
    try {
      client.copyToContainer(id, inputStream, internalPath);
    } catch (Exception ex) {
      throw new RuntimeException(ex);
    }
  }

}
