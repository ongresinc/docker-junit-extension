/*-
 *  § 
 * docker-junit-extension
 *    
 * Copyright (C) 2019 OnGres, Inc.
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * § §
 */

package com.ongres.junit.docker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.Preconditions;
import com.ongres.junit.docker.Mount.DefaultReference;
import org.jooq.lambda.Unchecked;
import org.junit.jupiter.api.extension.ExtensionContext;

@JsonDeserialize(builder = Instance.Builder.class)
public class Instance {
  public final Long index;
  public final String image;
  public final Optional<String> alias;
  public final List<String> arguments;
  public final Map<String, String> environment;
  public final Set<PortBinding> ports;
  public final List<MountBinding> mounts;

  public final String configuredAlias;
  public final WhenReuse whenReuse;
  public final boolean stopIfChanged;
  public final String[] expectedLogs;
  public final int expectedLogTimeout;
  public final int retry;

  private Instance(Builder builder) {
    this(builder.index, builder.image, builder.alias, builder.arguments, builder.environment,
        builder.ports, builder.mounts, builder.configuredAlias, builder.whenReuse,
        builder.stopIfChanged, builder.expectedLogs, builder.expectedLogTimeout, builder.retry);
  }

  protected static Instance fromAnnotation(ExtensionContext context, Long index,
      DockerContainer containerAnnotation) {
    String image = AnnotationUtils.getImage(containerAnnotation);
    Optional<String> alias = AnnotationUtils.getAlias(containerAnnotation);
    List<String> arguments = AnnotationUtils.getArguments(containerAnnotation);
    Map<String, String> environment = AnnotationUtils.getEnvironment(containerAnnotation);
    Set<PortBinding> ports = AnnotationUtils.getPortBindings(containerAnnotation);
    List<MountBinding> mounts = AnnotationUtils.getMounts(context, containerAnnotation);
    WaitFor waitFor = AnnotationUtils.getWaitFor(containerAnnotation);
    int retry = AnnotationUtils.getRetry(containerAnnotation);

    return new Instance(index, image, alias, arguments, environment, ports, mounts,
        containerAnnotation.alias(), containerAnnotation.whenReuse(),
        containerAnnotation.stopIfChanged(), waitFor.value(), waitFor.timeout(), retry);
  }

  private Instance(Long index, String image, Optional<String> alias, List<String> arguments,
      Map<String, String> environment, Set<PortBinding> ports, List<MountBinding> mounts,
      String configuredAlias, WhenReuse whenReuse, boolean stopIfChanged,
      String[] expectedLogs, int expectedLogTimeout, int retry) {
    Preconditions.checkNotNull(image);
    Preconditions.checkArgument(!image.isEmpty());
    Preconditions.checkNotNull(alias);
    Preconditions.checkNotNull(arguments);
    Preconditions.checkNotNull(environment);
    Preconditions.checkNotNull(ports);
    Preconditions.checkNotNull(mounts);
    Preconditions.checkNotNull(configuredAlias);
    Preconditions.checkNotNull(whenReuse);
    Preconditions.checkNotNull(expectedLogs);
    Preconditions.checkNotNull(expectedLogTimeout);
    Preconditions.checkArgument(retry >= 0);

    this.index = index;
    this.image = image;
    this.alias = alias;
    this.arguments = arguments;
    this.environment = environment;
    this.ports = ports;
    this.mounts = mounts;
    this.configuredAlias = configuredAlias;
    this.whenReuse = whenReuse;
    this.stopIfChanged = stopIfChanged;
    this.expectedLogs = expectedLogs;
    this.expectedLogTimeout = expectedLogTimeout;
    this.retry = retry;
  }

  @Override
  public int hashCode() {
    return Objects.hash(arguments, environment,
        image, index, mounts, ports);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (!(obj instanceof Instance)) {
      return false;
    }
    Instance other = (Instance) obj;
    return Objects.equals(arguments, other.arguments)
        && Objects.equals(environment, other.environment)
        && Objects.equals(image, other.image)
        && Objects.equals(index, other.index)
        && Objects.equals(mounts, other.mounts)
        && Objects.equals(ports, other.ports);
  }

  public Long getIndex() {
    return index;
  }

  public String getImage() {
    return image;
  }

  public Optional<String> getAlias() {
    return alias;
  }

  public List<String> getArguments() {
    return arguments;
  }

  public Map<String, String> getEnvironment() {
    return environment;
  }

  public Set<PortBinding> getPorts() {
    return ports;
  }

  public List<MountBinding> getMounts() {
    return mounts;
  }

  public String getConfiguredAlias() {
    return configuredAlias;
  }

  public WhenReuse getWhenReuse() {
    return whenReuse;
  }

  public boolean isStopIfChanged() {
    return stopIfChanged;
  }

  public String[] getExpectedLogs() {
    return expectedLogs;
  }

  public int getExpectedLogTimeout() {
    return expectedLogTimeout;
  }

  public int getRetry() {
    return retry;
  }

  /**
   * Creates builder to build {@link Instance}.
   * @return created builder
   */
  public static Builder builder() {
    return new Builder();
  }

  /**
   * Builder to build {@link Instance}.
   */
  public static final class Builder {
    private Long index;
    private String image = "";
    private Optional<String> alias = Optional.empty();
    private List<String> arguments = new ArrayList<>();
    private Map<String, String> environment = new HashMap<>();
    private Set<PortBinding> ports = new HashSet<>();
    private List<MountBinding> mounts = new ArrayList<>();
    private String configuredAlias = "";
    private WhenReuse whenReuse = WhenReuse.ALWAYS;
    private boolean stopIfChanged = false;
    private String[] expectedLogs = WaitFor.NOTHING;
    private int expectedLogTimeout = WaitFor.DEFAULT_TIMEOUT;
    private int retry = 3;

    private Builder() {
    }

    public Builder withIndex(Long index) {
      this.index = index;
      return this;
    }

    public Builder withImage(String image) {
      this.image = image;
      return this;
    }

    public Builder withAlias(Optional<String> alias) {
      this.alias = alias;
      return this;
    }

    public Builder withArguments(List<String> arguments) {
      this.arguments = arguments;
      return this;
    }

    @JsonIgnore
    public Builder withArgs(String...args) {
      this.arguments = Arrays.asList(args);
      return this;
    }

    public Builder withEnvironment(Map<String, String> environment) {
      this.environment = environment;
      return this;
    }

    public Builder withMounts(List<MountBinding> mounts) {
      this.mounts = mounts;
      return this;
    }

    public Builder withPorts(Set<PortBinding> ports) {
      this.ports = ports;
      return this;
    }

    public Builder withConfiguredAlias(String configuredAlias) {
      this.configuredAlias = configuredAlias;
      return this;
    }

    public Builder withWhenReuse(WhenReuse whenReuse) {
      this.whenReuse = whenReuse;
      return this;
    }

    public Builder withStopIfChanged(boolean stopIfChanged) {
      this.stopIfChanged = stopIfChanged;
      return this;
    }

    public Builder withExpectedLogs(String[] expectedLogs) {
      this.expectedLogs = expectedLogs;
      return this;
    }

    public Builder withExpectedLogTimeout(int expectedLogTimeout) {
      this.expectedLogTimeout = expectedLogTimeout;
      return this;
    }

    public Builder withRetry(int retry) {
      this.retry = retry;
      return this;
    }

    public Instance build() {
      return new Instance(this);
    }
  }

  private static final class AnnotationUtils {
    private static String getImage(DockerContainer containerAnnotation) {
      if (containerAnnotation.image().isEmpty()) {
        Optional<DockerContainer> superContainerAnnotation = getConfig(containerAnnotation);

        if (!superContainerAnnotation.isPresent()
            || superContainerAnnotation.get().image().isEmpty()) {
          throw new IllegalArgumentException("No image specified");
        }

        return getImage(superContainerAnnotation.get());
      }

      return containerAnnotation.image();
    }

    private static Optional<String> getAlias(DockerContainer containerAnnotation) {
      return Optional.ofNullable(containerAnnotation.alias()).filter(alias -> !alias.equals(""));
    }

    private static List<String> getArguments(DockerContainer containerAnnotation) {
      Optional<DockerContainer> superContainerAnnotation = getConfig(containerAnnotation);

      if (superContainerAnnotation.isPresent()) {
        return getArguments(superContainerAnnotation.get());
      }

      return Arrays.asList(containerAnnotation.arguments());
    }

    private static Map<String, String> getEnvironment(DockerContainer containerAnnotation) {
      Optional<DockerContainer> superContainerAnnotation = getConfig(containerAnnotation);

      Map<String, String> environmentVariables = new HashMap<>();

      if (superContainerAnnotation.isPresent()) {
        environmentVariables.putAll(getEnvironment(superContainerAnnotation.get()));
      }

      environmentVariables.putAll(Arrays.asList(containerAnnotation.environments())
          .stream()
          .collect(Collectors.toMap(e -> e.key(), e -> e.value())));

      return environmentVariables;
    }

    private static List<MountBinding> getMounts(ExtensionContext context,
        DockerContainer containerAnnotation) {
      Optional<DockerContainer> superContainerAnnotation = getConfig(containerAnnotation);

      List<MountBinding> mounts = new ArrayList<>();

      if (superContainerAnnotation.isPresent()) {
        mounts.addAll(getMounts(context, superContainerAnnotation.get()));
      }

      mounts.addAll(Arrays.asList(containerAnnotation.mounts())
          .stream()
          .map(Unchecked.function(mount -> MountBinding.builder()
              .withReference(mount.reference() == DefaultReference.class
                  ? context.getRequiredTestClass().getName() : mount.reference().getName())
              .withResource(mount.value())
              .withTemp(Mount.DEFAULT_TEMP.equals(mount.temp()) ? null : mount.temp())
              .withPath(mount.path())
              .withSystem(mount.system())
              .build()))
          .collect(Collectors.toList()));

      return mounts;
    }

    private static Set<PortBinding> getPortBindings(
        DockerContainer containerAnnotation) {
      Optional<DockerContainer> superContainerAnnotation = getConfig(containerAnnotation);

      Set<PortBinding> portBindings = new HashSet<>();

      if (superContainerAnnotation.isPresent()) {
        portBindings.addAll(getPortBindings(superContainerAnnotation.get()));
      }

      portBindings.addAll(Arrays.asList(containerAnnotation.ports())
          .stream()
          .map(port -> new PortBinding(port.protocol(), port.internal(),
              Optional.ofNullable(port.external())
              .filter(p -> p.intValue() != -1)
              .orElse(null)))
          .collect(Collectors.toList()));

      return portBindings;
    }

    private static WaitFor getWaitFor(DockerContainer containerAnnotation) {
      Optional<DockerContainer> superContainerAnnotation = getConfig(containerAnnotation);

      if (superContainerAnnotation.isPresent()) {
        return getWaitFor(superContainerAnnotation.get());
      }

      return containerAnnotation.waitFor();
    }

    private static int getRetry(DockerContainer containerAnnotation) {
      Optional<DockerContainer> superContainerAnnotation = getConfig(containerAnnotation);

      if (superContainerAnnotation.isPresent()) {
        return getRetry(superContainerAnnotation.get());
      }

      return containerAnnotation.retry();
    }

    private static Optional<DockerContainer> getConfig(DockerContainer containerAnnotation) {
      if (containerAnnotation.extendedBy() == Void.class) {
        return Optional.empty();
      }

      DockerContainer overrideContainerAnnotation = containerAnnotation
          .extendedBy().getAnnotation(DockerContainer.class);
      Preconditions.checkNotNull(overrideContainerAnnotation,
          "Class " + containerAnnotation.extendedBy().getName()
          + " is not annotated with @" + DockerContainer.class.getName());

      if (overrideContainerAnnotation.extendedBy() == containerAnnotation.extendedBy()) {
        return Optional.empty();
      }

      return Optional.of(overrideContainerAnnotation);
    }
  }
}
