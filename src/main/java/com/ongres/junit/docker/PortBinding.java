/*-
 *  § 
 * docker-junit-extension
 *    
 * Copyright (C) 2019 OnGres, Inc.
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * § §
 */

package com.ongres.junit.docker;

import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.Preconditions;

@JsonDeserialize(builder = PortBinding.Builder.class)
public class PortBinding {

  public final NetworkProtocol networkProtocol;
  public final Integer internalPort;
  public final Integer externalPort;

  private PortBinding(Builder builder) {
    this(builder.networkProtocol, builder.internalPort, builder.externalPort);
  }

  protected PortBinding(NetworkProtocol networkProtocol, Integer internalPort,
      Integer externalPort) {
    Preconditions.checkNotNull(networkProtocol);
    Preconditions.checkNotNull(internalPort);
    Preconditions.checkArgument(internalPort > 0, "Internal port must be > 0");
    Preconditions.checkArgument(externalPort == null || externalPort > 0,
        "External port must be > 0");

    this.networkProtocol = networkProtocol;
    this.internalPort = internalPort;
    this.externalPort = externalPort;
  }

  public NetworkProtocol getNetworkProtocol() {
    return networkProtocol;
  }

  public Integer getInternalPort() {
    return internalPort;
  }

  public Optional<Integer> getExternalPort() {
    return Optional.ofNullable(externalPort);
  }

  @JsonIgnore
  public String getDockerIdentifier() {
    return internalPort + "/" + networkProtocol.getName();
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((internalPort == null) ? 0 : internalPort.hashCode());
    result = prime * result + ((networkProtocol == null) ? 0 : networkProtocol.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    PortBinding other = (PortBinding) obj;
    if (internalPort == null) {
      if (other.internalPort != null) {
        return false;
      }
    } else if (!internalPort.equals(other.internalPort)) {
      return false;
    }
    if (networkProtocol != other.networkProtocol) {
      return false;
    }
    return true;
  }

  /**
   * Creates builder to build {@link PortBinding}.
   * @return created builder
   */
  public static Builder builder() {
    return new Builder();
  }

  /**
   * Builder to build {@link PortBinding}.
   */
  public static final class Builder {
    private NetworkProtocol networkProtocol;
    private Integer internalPort;
    private Integer externalPort = null;

    private Builder() {
    }

    public Builder withNetworkProtocol(NetworkProtocol networkProtocol) {
      this.networkProtocol = networkProtocol;
      return this;
    }

    public Builder withInternalPort(Integer internalPort) {
      this.internalPort = internalPort;
      return this;
    }

    public Builder withExternalPort(Integer externalPort) {
      this.externalPort = externalPort;
      return this;
    }

    public PortBinding build() {
      return new PortBinding(this);
    }
  }
}
