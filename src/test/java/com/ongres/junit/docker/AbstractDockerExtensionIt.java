/*-
 *  § 
 * docker-junit-extension
 *    
 * Copyright (C) 2019 OnGres, Inc.
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * § §
 */

package com.ongres.junit.docker;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Arrays;
import java.util.regex.Pattern;

import com.google.common.collect.ImmutableList;
import org.apache.commons.io.FileUtils;
import org.jooq.lambda.Seq;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractDockerExtensionIt {

  private static final Logger LOGGER = LoggerFactory.getLogger(AbstractDockerExtensionIt.class);

  @Test
  public void externalPortTest(@ContainerParam("postgres") Container postgres) throws Exception {
    try (Connection connection = DriverManager.getConnection(
        "jdbc:postgresql://172.17.0.1:" + postgres.getPort(5432) + "/postgres?user=postgres");
        Statement s = connection.createStatement();
        ResultSet rs = s.executeQuery("SELECT pg_is_in_recovery()")) {
      Assertions.assertTrue(rs.next());
      Assertions.assertFalse(rs.getBoolean(1));
    }
  }

  @Test
  public void ipTest(@ContainerParam("postgres") Container postgres) throws Exception {
    try (Connection connection = DriverManager.getConnection(
        "jdbc:postgresql://" + postgres.getIp() + ":5432/postgres?user=postgres");
        Statement s = connection.createStatement();
        ResultSet rs = s.executeQuery("SELECT pg_is_in_recovery()")) {
      Assertions.assertTrue(rs.next());
      Assertions.assertFalse(rs.getBoolean(1));
    }
  }

  @Test
  public void execTest(@ContainerParam("postgres") Container postgres) throws Exception {
    Assertions.assertIterableEquals(
        Arrays.asList(1, 2, 3),
        Seq.seq(postgres.execute("sh", "-c", "echo 1 2 3 | tr ' ' '\n'"))
          .peek(line -> LOGGER.debug(line))
          .map(line -> Integer.parseInt(line))
          .toList());
  }

  @Test
  public void execFailTest(@ContainerParam("postgres") Container postgres) throws Exception {
    try {
      Seq.seq(postgres.execute("sh", "-c", "echo 1; false"))
          .zipWithIndex()
          .peek(t -> {
            if (t.v2.longValue() == 0L) {
              Assertions.assertEquals("1", t.v1);
            }
          })
          .toList();
      Assertions.fail();
    } catch (RuntimeException ex) {
      Assertions.assertTrue(Pattern.matches(
          execFailTestExceptionMessagePattern(), ex.getMessage()),
          "Expected pattern " + execFailTestExceptionMessagePattern()
          + " but was " + ex.getMessage());
    }
  }

  protected String execFailTestExceptionMessagePattern() {
    return Pattern.quote("Command sh -c echo 1; false exited with code 1");
  }

  @Test
  public void mountTest(@ContainerParam("postgres") Container postgres) throws Exception {
    Seq.seq(postgres.execute("sh", "-c", "ls /classpath/log4j2.xml"))
      .peek(line -> LOGGER.debug(line))
      .peek(line -> Assertions.assertEquals("/classpath/log4j2.xml", line))
        .toList();
  }

  @Test
  public void mountFromJarTest(@ContainerParam("postgres") Container postgres) throws Exception {
    Seq.seq(postgres.execute("sh", "-c", "ls /classpath-jar/Log4j-charsets.properties"))
      .peek(line -> LOGGER.debug(line))
      .peek(line -> Assertions.assertEquals("/classpath-jar/Log4j-charsets.properties", line))
        .toList();
  }

  @Test
  public void runTest(@DockerParam Docker docker) throws Exception {
    Assertions.assertIterableEquals(
        Arrays.asList(1, 2, 3),
        Seq.seq(docker.run(Instance.builder().withImage("postgres:11").build(),
            "sh", "-c", "echo 1 2 3 | tr ' ' '\n'"))
          .peek(line -> LOGGER.debug(line))
          .map(line -> Integer.parseInt(line))
          .toList());
  }

  @Test
  public void runFailTest(@DockerParam Docker docker) throws Exception {
    try {
      Seq.seq(docker.run(Instance.builder().withImage("postgres:11").build(),
          "sh", "-c", "echo 1; false"))
        .peek(line -> Assertions.assertEquals("1", line))
          .toList();
      Assertions.fail();
    } catch (RuntimeException ex) {
      Assertions.assertTrue(Pattern.matches(
          execFailTestExceptionMessagePattern(), ex.getMessage()),
          "Expected pattern " + execFailTestExceptionMessagePattern()
          + " but was " + ex.getMessage());
    }
  }

  @Test
  public void copyFromInputStreamTest(@ContainerParam("postgres") Container postgres)
      throws Exception {
    Seq.seq(postgres.execute("sh", "-c", "rm -Rf /classpath2"))
        .forEach(line -> LOGGER.debug(line));
    postgres.copyIn(AbstractDockerExtensionIt.class.getResourceAsStream("/log4j2.xml"),
        "/classpath2/log4j2.xml");
    Seq.seq(postgres.execute("sh", "-c", "ls /classpath2/log4j2.xml"))
      .peek(line -> LOGGER.debug(line))
      .peek(line -> Assertions.assertEquals("/classpath2/log4j2.xml", line))
        .toList();
  }

  @Test
  public void copyFromPathTest(@ContainerParam("postgres") Container postgres) throws Exception {
    Seq.seq(postgres.execute("sh", "-c", "rm -Rf /classpath2"))
        .forEach(line -> LOGGER.debug(line));
    postgres.copyIn(Paths.get(AbstractDockerExtensionIt.class.getResource("/log4j2.xml")
        .toURI()).getParent(),
        "/classpath2");
    Seq.seq(postgres.execute("sh", "-c", "ls /classpath2/log4j2.xml"))
      .peek(line -> LOGGER.debug(line))
      .peek(line -> Assertions.assertEquals("/classpath2/log4j2.xml", line))
        .toList();
  }

  @Test
  public void copyFromResourceTest(@ContainerParam("postgres") Container postgres)
      throws Exception {
    Seq.seq(postgres.execute("sh", "-c", "rm -Rf /classpath2"))
        .forEach(line -> LOGGER.debug(line));
    postgres.copyResourceIn("/log4j2.xml", AbstractDockerExtensionIt.class,
        "/classpath2");
    Seq.seq(postgres.execute("sh", "-c", "ls /classpath2/log4j2.xml"))
      .peek(line -> LOGGER.debug(line))
      .peek(line -> Assertions.assertEquals("/classpath2/log4j2.xml", line))
        .toList();
  }

  @Test
  public void copyFromResourcesTest(@ContainerParam("postgres") Container postgres)
      throws Exception {
    Seq.seq(postgres.execute("sh", "-c", "rm -Rf /classpath2"))
        .forEach(line -> LOGGER.debug(line));
    postgres.copyResourcesIn("/", AbstractDockerExtensionIt.class,
        "/classpath2");
    Seq.seq(postgres.execute("sh", "-c", "ls /classpath2/log4j2.xml"))
      .peek(line -> LOGGER.debug(line))
      .peek(line -> Assertions.assertEquals("/classpath2/log4j2.xml", line))
        .toList();
  }

  @Test
  public void copyFileToPathTest(@ContainerParam("postgres") Container postgres) throws Exception {
    Seq.seq(postgres.execute("sh", "-c", "rm -Rf /classpath3; mkdir -p /classpath3;"
        + " echo hola > /classpath3/test"))
        .forEach(line -> LOGGER.debug(line));
    FileUtils.deleteQuietly(Paths.get("target/classpath3-test").toFile());
    postgres.copyOut("/classpath3/test", Paths.get("target/classpath3-test"));
    Assertions.assertTrue(Paths.get("target/classpath3-test").toFile().exists());
    Assertions.assertTrue(Paths.get("target/classpath3-test").toFile().isFile());
    Assertions.assertIterableEquals(Files.readAllLines(Paths.get("target/classpath3-test")),
        ImmutableList.of("hola"));
  }

  @Test
  public void copyDirectoryToPathTest(@ContainerParam("postgres") Container postgres)
      throws Exception {
    Seq.seq(postgres.execute("sh", "-c", "rm -Rf /classpath4; mkdir -p /classpath4/test2;"
        + " echo hola > /classpath4/test; echo hola2 > /classpath4/test2/test3;"
        + " ln -s /classpath4/test2/test3 /classpath4/test2/test4"))
        .forEach(line -> LOGGER.debug(line));
    if (Paths.get("target/classpath4-test").toFile().exists()) {
      FileUtils.deleteQuietly(Paths.get("target/classpath4-test").toFile());
    }
    postgres.copyOut("/classpath4", Paths.get("target/classpath4-test"));
    Assertions.assertTrue(Paths.get("target/classpath4-test/test").toFile().exists());
    Assertions.assertTrue(Paths.get("target/classpath4-test/test").toFile().isFile());
    Assertions.assertIterableEquals(Files.readAllLines(Paths.get("target/classpath4-test/test")),
        ImmutableList.of("hola"));
    Assertions.assertTrue(Paths.get("target/classpath4-test/test2/test3").toFile().exists());
    Assertions.assertTrue(Paths.get("target/classpath4-test/test2/test3").toFile().isFile());
    Assertions.assertIterableEquals(
        Files.readAllLines(Paths.get("target/classpath4-test/test2/test3")),
        ImmutableList.of("hola2"));
    Assertions.assertFalse(Paths.get("target/classpath4-test/test2/test4").toFile().exists());
  }
}
