/*-
 *  § 
 * docker-junit-extension
 *    
 * Copyright (C) 2019 OnGres, Inc.
 *    
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * § §
 */

package com.ongres.junit.docker;

import org.slf4j.Logger;

@DockerContainer(
    image = "postgres:11",
    ports = { @Port(internal = 5432) },
    waitFor = @WaitFor(timeout = 300_000_000, value = {
      "PostgreSQL init process complete; ready for start up.",
      "database system is ready to accept connections"
    }),
    environments = { @Environment(key = "POSTGRES_HOST_AUTH_METHOD", value = "trust") },
    mounts = {
        @Mount(value = "@/log4j2.xml", path = "/classpath"),
        @Mount(value = "@/Log4j-charsets.properties",
            path = "/classpath-jar", reference = Logger.class)
    })
public interface DockerExtensionConfiguration {
}
